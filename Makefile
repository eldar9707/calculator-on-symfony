##
## Checking the existence of a local environment file, to redefine ports and other settings
## ----------------------------------------------------------------------------------------
ENV_FILE = '.env'
ifneq ("$(wildcard .env.local)","")
    ENV_FILE := '.env.local'
endif

DOCKER_COMPOSE = docker compose --env-file $(ENV_FILE)
PHP_CLI        = $(DOCKER_COMPOSE) run --rm php-fpm
SYMFONY        = $(PHP_CLI) symfony

getargs    = $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
escapeagrs = $(subst :,\:,$(1))

.PHONY: dummy migrations

init: docker-down docker-up php-fpm-composer-install
down: docker-down
docker-up:
	$(DOCKER_COMPOSE) up -d --build
docker-down:
	$(DOCKER_COMPOSE) down --remove-orphans
php-fpm-composer-install:
	$(DOCKER_COMPOSE) exec php-fpm composer install

##
## Run Symfony CLI
## ---------------------------------------------------------------------------------------------------------------
ifeq (sf,$(firstword $(MAKECMDGOALS)))
    SYMFONY_ARGS         := $(call getargs)
    SYMFONY_ARGS_ESCAPED := $(call escapeagrs, $(SYMFONY_ARGS))
    $(eval $(SYMFONY_ARGS_ESCAPED):dummy;@:)
endif
sf:
	$(SYMFONY) $(SYMFONY_ARGS) $(-*-command-variables-*-)
