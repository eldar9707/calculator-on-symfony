First install [docker](https://docs.docker.com/engine/install/)

Run `make init`, and you'll be ready to go.

To start Docker containers, use `make docker-up`.
To stop Docker containers, use `make docker-down`.
