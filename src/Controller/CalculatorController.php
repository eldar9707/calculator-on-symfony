<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\CalculatorForm;
use App\Service\CalculatorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CalculatorController extends AbstractController
{
    public function __construct(
        private readonly CalculatorService $calculatorService
    ) {
    }

    #[Route('/', name: 'home')]
    public function index(Request $request): Response
    {
        $form = $this->createForm(CalculatorForm::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $calculator = $this->calculatorService->calculate($form->getData());
        }

        return $this->render('calculator/index.html.twig', [
            'form'       => $form,
            'calculator' => $calculator ?? null,
        ]);
    }
}
