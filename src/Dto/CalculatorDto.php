<?php

declare(strict_types=1);

namespace App\Dto;

use App\Enum\OperationEnum;

class CalculatorDto
{
    private float $firstArgument;
    private float $secondArgument;
    private float $result;
    private OperationEnum $operation;

    public function __toString(): string
    {
        return \sprintf(
            '%s %s %s = %s',
            $this->firstArgument,
            $this->operation->value,
            $this->secondArgument,
            $this->result,
        );
    }

    public function getFirstArgument(): float
    {
        return $this->firstArgument;
    }

    public function setFirstArgument(float $firstArgument): CalculatorDto
    {
        $this->firstArgument = $firstArgument;
        return $this;
    }

    public function getSecondArgument(): float
    {
        return $this->secondArgument;
    }

    public function setSecondArgument(float $secondArgument): CalculatorDto
    {
        $this->secondArgument = $secondArgument;
        return $this;
    }

    public function getOperation(): OperationEnum
    {
        return $this->operation;
    }

    public function setOperation(OperationEnum $operation): CalculatorDto
    {
        $this->operation = $operation;
        return $this;
    }

    public function getResult(): float
    {
        return $this->result;
    }

    public function setResult(float $result): CalculatorDto
    {
        $this->result = $result;
        return $this;
    }
}