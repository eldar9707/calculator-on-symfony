<?php

declare(strict_types=1);

namespace App\Enum;

enum OperationEnum: string
{
    case Addition = '+';
    case Subtraction = '-';
    case Multiplication = '*';
    case Division = '/';
}
