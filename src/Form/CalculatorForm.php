<?php

declare(strict_types=1);

namespace App\Form;

use App\Dto\CalculatorDto;
use App\Enum\OperationEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class CalculatorForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('first_argument', NumberType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(max: 14)
                ],
            ])
            ->add('second_argument', NumberType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(max: 14),
                    new Assert\Callback(function ($value, $context) {
                        if ($context->getRoot()->get('operation')->getData() === OperationEnum::Division && (int) $value === 0) {
                            $context->buildViolation('Cannot divide by zero')->addViolation();
                        }
                    }),
                ],
            ])
            ->add('operation', EnumType::class, [
                'class' => OperationEnum::class,
                'choice_label' => fn ($operation) => $operation->value,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CalculatorDto::class,
        ]);
    }
}
