<?php

declare(strict_types=1);

namespace App\Service;

use App\Dto\CalculatorDto;
use App\Enum\OperationEnum;

class CalculatorService
{
    public function calculate(CalculatorDto $calculator): CalculatorDto
    {
        $this->validate($calculator);

        $result = match ($calculator->getOperation()) {
            OperationEnum::Addition       => $calculator->getFirstArgument() + $calculator->getSecondArgument(),
            OperationEnum::Subtraction    => $calculator->getFirstArgument() - $calculator->getSecondArgument(),
            OperationEnum::Multiplication => $calculator->getFirstArgument() * $calculator->getSecondArgument(),
            OperationEnum::Division       => $calculator->getFirstArgument() / $calculator->getSecondArgument(),
        };

        return $calculator->setResult($result);
    }

    private function validate(CalculatorDto $calculator): void
    {
        if ($calculator->getOperation() === OperationEnum::Division && (int) $calculator->getSecondArgument() === 0) {
            throw new \InvalidArgumentException('Cannot divide by zero');
        }
    }
}