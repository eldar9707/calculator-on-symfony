<?php

declare(strict_types=1);

namespace App\Tests;

use App\Dto\CalculatorDto;
use App\Enum\OperationEnum;
use App\Service\CalculatorService;
use PHPUnit\Framework\TestCase;

class CalculatorServiceTest extends TestCase
{
    public function calculationDataProvider(): \Generator
    {
        yield 'Addition'       => [5, 3, OperationEnum::Addition, 8.0];
        yield 'Subtraction'    => [5, 3, OperationEnum::Subtraction, 2.0];
        yield 'Multiplication' => [5, 3, OperationEnum::Multiplication, 15.0];
        yield 'Division'       => [6, 2, OperationEnum::Division, 3.0];
        yield 'DivisionByZero' => [6, 0, OperationEnum::Division, null];
    }

    /**
     * @dataProvider calculationDataProvider
     */
    public function testCalculate(float $firstArgument, float $secondArgument, OperationEnum $operation, ?float $expectedResult): void
    {
        $calculator = (new CalculatorDto())
            ->setFirstArgument($firstArgument)
            ->setSecondArgument($secondArgument)
            ->setOperation($operation);

        $service = new CalculatorService();

        if ($expectedResult === null) {
            $this->expectException(\InvalidArgumentException::class);
            $this->expectExceptionMessage('Cannot divide by zero');
        }

        $result = $service->calculate($calculator);

        if ($expectedResult !== null) {
            $this->assertSame($expectedResult, $result->getResult());
        }
    }
}
